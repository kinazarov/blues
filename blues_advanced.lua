function start(args)
	local cmp = require("component")
	local r = cmp.robot
	local pc = require("computer")
	local event = require("event")
	local charge_check_timeout = 5

	local function colorRGB(r, g ,b)
		return b + g*256 + r*256*256
	end

	local colors = {
		[0] = colorRGB(96, 47, 1),
		colorRGB(100, 114, 10),
		colorRGB(22, 153, 135),
		colorRGB(11, 123, 221)
	}

	local function currentPowerStep()
		--[[if pc.energy is 0 then pc will be shut down
		so I do not care if it can possibly
		crash or fail to display right color on this power level]]
		return math.floor((pc.energy() - 1) / pc.maxEnergy() * #colors)
	end

	local function setIndicator()
		r.setLightColor(colors[currentPowerStep()])
	end

	event.timer(charge_check_timeout, setIndicator, math.huge)
end